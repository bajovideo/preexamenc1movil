package com.example.preexamenc1movil;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class Recibo extends AppCompatActivity {

    private EditText nombreEdit, horasTrabajadas, horasExtras, subtotal, impuesto, totalPagar;
    private RadioGroup puestoRadioGroup;
    private TextView nombreTrabajador, numeroRecibo;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        // Obtener referencias a los elementos de la UI
        nombreTrabajador = findViewById(R.id.nombreTrabajador);
        numeroRecibo = findViewById(R.id.numeroRecibo);
        nombreEdit = findViewById(R.id.nombreEdit);
        horasTrabajadas = findViewById(R.id.horasTrabajadas);
        horasExtras = findViewById(R.id.horasExtras);
        puestoRadioGroup = findViewById(R.id.puestoRadioGroup);
        subtotal = findViewById(R.id.subtotal);
        impuesto = findViewById(R.id.impuesto);
        totalPagar = findViewById(R.id.totalPagar);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Recibir el nombre del trabajador desde la MainActivity
        String nombre = getIntent().getStringExtra("nombreTrabajador");
        nombreTrabajador.setText(nombre);
        nombreEdit.setText(nombre);

        // Generar un número de recibo aleatorio
        Random random = new Random();
        int numRecibo = random.nextInt(1000) + 1; // Número aleatorio entre 1 y 1000
        numeroRecibo.setText(String.valueOf(numRecibo));

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularNomina();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularNomina() {
        try {
            float horasTrabNormal = Float.parseFloat(horasTrabajadas.getText().toString());
            float horasTrabExtras = Float.parseFloat(horasExtras.getText().toString());
            float pagoBase = 200;

            int puestoSeleccionadoId = puestoRadioGroup.getCheckedRadioButtonId();
            if (puestoSeleccionadoId == -1) {
                throw new Exception("Por favor seleccione un puesto");
            }

            float incremento = 0;
            if (puestoSeleccionadoId == R.id.radioAuxiliar) {
                incremento = 0.20f;
            } else if (puestoSeleccionadoId == R.id.radioAlbanil) {
                incremento = 0.50f;
            } else if (puestoSeleccionadoId == R.id.radioIngObra) {
                incremento = 1.00f;
            }

            float pagoPorHora = pagoBase + (pagoBase * incremento);
            float subtotalCalculado = (horasTrabNormal * pagoPorHora) + (horasTrabExtras * pagoPorHora * 2);
            float impuestoCalculado = subtotalCalculado * 0.16f;
            float totalCalculado = subtotalCalculado - impuestoCalculado;

            subtotal.setText(String.format("%.2f", subtotalCalculado));
            impuesto.setText(String.format("%.2f", impuestoCalculado));
            totalPagar.setText(String.format("%.2f", totalCalculado));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            // Manejar error de formato numérico aquí (mostrar mensaje al usuario, etc.)
        } catch (Exception e) {
            e.printStackTrace();
            // Manejar otras excepciones aquí (mostrar mensaje al usuario, etc.)
        }
    }

    private void limpiarCampos() {
        horasTrabajadas.setText("");
        horasExtras.setText("");
        puestoRadioGroup.clearCheck();
        subtotal.setText("");
        impuesto.setText("");
        totalPagar.setText("");
    }
}