package com.example.preexamenc1movil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etNombreTrabajador;
    private Button btnEntrar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombreTrabajador = findViewById(R.id.etNombreTrabajador);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = etNombreTrabajador.getText().toString().trim();
                if (!nombre.isEmpty()) {
                    Intent intent = new Intent(MainActivity.this, Recibo.class);
                    intent.putExtra("nombreTrabajador", nombre);
                    startActivity(intent);
                } else {
                    etNombreTrabajador.setError("Por favor, ingrese el nombre del trabajador");
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}