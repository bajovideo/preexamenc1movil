public class reciboNomina {
    private int numRecibo;
    private int puesto;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private String nombre;
    private float porcentajeImpuesto;

    public reciboNomina(int numRecibo, int puesto, float horasTrabNormal, float horasTrabExtras, String nombre, float porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.puesto = puesto;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.nombre = nombre;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public reciboNomina() {
        this.numRecibo = 0;
        this.puesto = 1; // Valor por defecto
        this.horasTrabNormal = 0.0f;
        this.horasTrabExtras = 0.0f;
        this.nombre = "";
        this.porcentajeImpuesto = 16.0f; // Valor por defecto
    }

    // Getters y Setters
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(float porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    // Método para calcular el salario total
    public float calcularSubtotal() {
        float pagoBase = 200.0f;
        float pagoPorHora = 0.0f;

        // Calcular el pago por hora según el puesto
        switch (puesto) {
            case 0: // Auxiliar
                pagoPorHora = pagoBase * 1.20f;
                break;
            case 1: // Albañil
                pagoPorHora = pagoBase * 1.50f;
                break;
            case 2: // Ing Obra
                pagoPorHora = pagoBase * 2.00f;
                break;
            default:
                throw new IllegalArgumentException("Puesto no válido");
        }

        // Calcular el subtotal
        return (horasTrabNormal * pagoPorHora) + (horasTrabExtras * pagoPorHora );
    }

    public float calcularImpuesto() {
        float subtotal = calcularSubtotal();
        return subtotal * (porcentajeImpuesto / 100);
    }

    public float calcularTotalPagar() {
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }

}
